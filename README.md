# [T3 - Computação Gráfica](https://gitlab.com/debemdeboas/pucrs-computacao-grafica-t3) - CC 2022/2 PUCRS

O trabalho foi desenvolvido em Python 3.10 em Windows 64-bit utilizando os pacotes não-oficiais
do `PyOpenGL` e `PyOpenGL_Accelerate`. Os arquivos `.whl` estão armazenados na pasta [`lib`](./lib).

## Autores

Isabella Do Prado Pagnoncelli, Rafael Almeida de Bem.

## Instalação dos pacotes

```commandline
$ python3.10 -m venv venv
$ ./venv/Scripts/activate
(venv) $ pip install --no-index --find-links=lib/ -r requirements.txt
```

## Relatório

Não houve tempo hábil para a conclusão do trabalho.
O carregamento dos arquivos .TRI deixou o programa proibitivamente lento.

Segue um print com 20 objetos TRI carregados.
Utilizamos [esse arquivo (`dog.tri`)](3dobjects/tri/dog.tri) como exemplo, em duas escalas.

![](./lib/img/Screenshot_3.png)

Implementamos uma função de colisão entre bounding boxes igual a do T1.
Entretanto, não foi possível modelar o tanque, tampouco o projétil.
A função de colisão, assim como a de trajetória do projétil, entre outras,
estão no arquivo [`physics.py`](src/physics.py).

Foi implementado um sistema de câmera movimentada pelo teclado.
[[WASD]] para translações, [[QE]] para ir para cima/baixo, e as setas para
fixar a câmera em vista superior ou vista lateral.
É possível utilizar o mouse para mudar o ângulo da câmera (pitch/yaw).
