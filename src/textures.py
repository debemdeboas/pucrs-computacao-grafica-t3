import numpy as np
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from PIL import Image

from src.globals import g_textures


def use_texture(texture_num: int):
    """
    Habilita o uso de textura `texture_num`

    Se `texture_num < 0`, desabilita o uso de texturas
    Se `texture_num` for maior que a quantidade de texturas, gera
    mensagem de erro e desabilita o uso de texturas
    """
    if texture_num > len(g_textures):
        print ("Numero invalido da textura.")
        glDisable(GL_TEXTURE_2D)
        return

    if texture_num < 0:
        glDisable(GL_TEXTURE_2D)
    else:
        glEnable(GL_TEXTURE_2D)
        glBindTexture(GL_TEXTURE_2D, g_textures[texture_num])


def load_texture(filename) -> int:
    """
    Retorna o ID da textura lida
    """

    img = Image.open(filename)
    img_data = np.array(list(img.getdata()), np.uint8)

    # Habilita o uso de textura
    glEnable(GL_TEXTURE_2D)

    #Cria um ID para textura
    texture = glGenTextures(1)
    error_code =  glGetError()
    if error_code == GL_INVALID_OPERATION:
        print ("Erro: glGenTextures chamada entre glBegin/glEnd.")
        return -1

    # Define a forma de armazenamento dos pixels na textura (1 = alinhamento por byte)
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1)
    # Define que tipo de textura ser usada
    # GL_TEXTURE_2D ==> define que ser· usada uma textura 2D (bitmaps)
    # e o nro dela
    glBindTexture(GL_TEXTURE_2D, texture)

    # texture wrapping params
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
    # texture filtering params
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)

    error_code = glGetError()
    if error_code != GL_NO_ERROR:
        print ("Houve algum erro na criação da textura.")
        return -1

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, img.size[0], img.size[1], 0, GL_RGB, GL_UNSIGNED_BYTE, img_data)
    # neste ponto, "texture" tem o nro da textura que foi carregada
    error_code = glGetError()
    if error_code == GL_INVALID_OPERATION:
        print ("Erro: glTexImage2D chamada entre glBegin/glEnd.")
        return -1

    if error_code != GL_NO_ERROR:
        print ("Houve algum erro na criação da textura.")
        return -1
    return texture
