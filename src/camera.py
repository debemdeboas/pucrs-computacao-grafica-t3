from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

from src.constants import Numeric
from src.vec3 import Vec3

import math

class Camera:
    def __init__(self) -> None:
        self.pos: Vec3 = Vec3(0, 0, 0)
        self.direction: Vec3 = Vec3(0, 0, 0)
        self.up: Vec3 = Vec3(0, 1, 0)
        self.left: Vec3 = Vec3(-1, 0, 0)
        self.speed: Vec3 = Vec3(0, 0, 0)
        self.yaw = 0
        self.pitch = 0
        self.scale = 1.1

        self.activated = False

    def activate(self):
        self.look_at(self.pos)
        if not self.activated:
            self.update()
            self.activated = True

    def unpack(self):
        return [self.pos.x, self.pos.y, self.pos.z,
                self.direction.x, self.direction.y, self.direction.z,
                self.up.x, self.up.y, self.up.z]

    def look_at(self, orientation: Vec3):
        look = self.direction + orientation
        gluLookAt(self.pos.x, self.pos.y, self.pos.z,
                  look.x, look.y, look.z,
                  self.up.x, self.up.y, self.up.z)

    def move_forward(self):
        self.speed = self.direction * self.scale
        self.pos += self.speed

    def move_backward(self):
        self.speed = self.direction * -self.scale
        self.pos += self.speed
        
    def move_left(self):
        self.speed = self.left * self.scale
        self.pos += self.speed

    def move_right(self):
        self.speed = self.left * -self.scale
        self.pos += self.speed

    def move_up(self):
        self.speed = self.up * self.scale
        self.pos += self.speed

    def move_down(self):
        self.speed = self.up * -self.scale
        self.pos += self.speed

    def update_yaw(self, yaw: float):
        self.yaw += yaw

    def update_pitch(self, pitch: float):
        self.pitch += pitch

    def update(self):
        x_angle_in_rad = math.radians(self.yaw)
        self.direction.x = math.sin(x_angle_in_rad)
        self.direction.z = -math.cos(x_angle_in_rad)

        y_angle_in_rad = math.radians(self.pitch)
        self.direction.y = -math.tan(y_angle_in_rad)

        self.direction.normalize()
        self.left = self.up.cross_product(self.direction)

    def set_pos(self, pos: Vec3, direction: Vec3 | None = None): 
        self.pos = pos
        if direction:
            self.direction = direction
        self.pitch = self.direction.y
        self.yaw = self.direction.x
