import time
# from src.models import Model, GameObject
# from src.polygon import WallBlock
from src.camera import Camera

g_textures = list()
g_models: list['Model'] = []
g_objects: list['GameObject'] = []
g_angle = 0.0
g_n_frames, g_total_time, g_delta_t_accum = 0, 0, 0
g_fps_cap = 60
g_old_time = time.time()
g_camera = Camera()
g_last_mouse_pos_x = 0.0
g_last_mouse_pos_y = 0.0
g_mouse_move_threshold = 15
g_division_wall: list['WallBlock'] = []

# Scene dimensions
MIN_WIDTH = 1
MAX_WIDTH = 50
MIN_DEPTH = 1
MAX_DEPTH = 25
MIN_HEIGHT = 0
MAX_HEIGHT = 4
DIV_WALL_HEIGHT = 8
WALL_HEIGHT = 4
