from dataclasses import dataclass

from src.vec3 import Vec3


@dataclass
class Triangle:
    p1: Vec3
    p2: Vec3
    p3: Vec3


@dataclass
class Polygon:
    center: Vec3 = Vec3()
    half_size: Vec3 = Vec3()
    pos: Vec3 = Vec3()


class BoundObject:
    def __init__(self, pos: Vec3) -> None:
        self.pos = pos
        self.bounding_box = Polygon()
        self.bounding_box.center = pos
        self.bounding_box.half_size = Vec3(1, 1, 1) / 2


class WallBlock(BoundObject):
    destroyed: bool = False
