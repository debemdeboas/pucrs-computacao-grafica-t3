
import math
from dataclasses import dataclass
from typing import Union

from src.constants import Numeric


@dataclass
class Vec3:
    x: Numeric
    y: Numeric
    z: Numeric
    
    def __init__(self, x: Numeric = 0, y: Numeric = 0, z: Numeric = 0):
        self.x = x
        self.y = y
        self.z = z

    @staticmethod
    def modulo(v: 'Vec3') -> float:
        return math.sqrt((v.x ** 2) + (v.y ** 2) + (v.z ** 2))

    def magnitude(self) -> float:
        return math.sqrt((self.x ** 2) + (self.y ** 2) + (self.z ** 2))

    def normalize(self):
        magnitude = self.magnitude()
        self.x /= magnitude
        self.y /= magnitude
        self.z /= magnitude

    def cross_product(self, v: 'Vec3') -> 'Vec3':
        return Vec3(
            (self.y * v.z) - (self.z * v.y),
            (self.z * v.x) - (self.x * v.z),
            (self.x * v.y) - (self.y * v.x)
        )

    def __add__(self, v: 'Vec3') -> 'Vec3':
        return Vec3(self.x + v.x,
                    self.y + v.y,
                    self.z + v.z)

    def __sub__(self, v: 'Vec3') -> 'Vec3':
        return Vec3(self.x - v.x,
                    self.y - v.y,
                    self.z - v.z)

    def __mul__(self, v: Union['Vec3', Numeric]) -> 'Vec3':
        if isinstance(v, Vec3):
            return Vec3(self.x * v.x,
                        self.y * v.y,
                        self.z * v.z)
        else:
            return Vec3(self.x * v,
                        self.y * v,
                        self.z * v)
        
    def __truediv__(self, v: Union['Vec3', Numeric]) -> 'Vec3':
        if isinstance(v, Vec3):
            return Vec3(self.x / v.x,
                        self.y / v.y,
                        self.z / v.z)
        else:
            return Vec3(self.x / v,
                        self.y / v,
                        self.z / v)
    
    def unpack(self) -> tuple[Numeric, Numeric, Numeric]:
        return (self.x, self.y, self.z)

