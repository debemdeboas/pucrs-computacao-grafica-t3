
from enum import Enum
from collections import namedtuple

Numeric = int | float
MinMax = namedtuple('MinMax', 'min max')
Algorithm = Enum('Algorithm', '_None BruteForce BoundingBox QuadTree')
Side = Enum('Side', 'Left Right Atop')

GRAVITY = -9.8
PROJECTILE_POWER = 2
