import math

from src.constants import GRAVITY, PROJECTILE_POWER, MinMax
from src.polygon import Polygon
from src.vec3 import Vec3


def inside_boundaries(bounding_box: Polygon,
                      pos: Vec3,
                      bounds_depth: MinMax,
                      bounds_width: MinMax) -> bool:
    top = pos.z - bounding_box.half_size.z
    bottom = pos.z + bounding_box.half_size.z
    left = pos.x - bounding_box.half_size.x
    right = pos.x + bounding_box.half_size.x
    
    if top <= -bounds_depth.max or bottom >= -bounds_depth.min or \
       left <= bounds_width.min or right >= bounds_width.max:
        return False
    return True


def check_collision(bb_1: Polygon, bb_2: Polygon):
    if abs(bb_1.center.x - bb_2.center.x) > bb_1.half_size.x + bb_2.half_size.x:
        return False
    if abs(bb_1.center.y - bb_2.center.y) > bb_1.half_size.y + bb_2.half_size.y:
        return False
    if abs(bb_1.center.z - bb_2.center.z) > bb_1.half_size.z + bb_2.half_size.z:
        return False
    return True


def projectile_trajectory(curr: Vec3, theta: float, time: float) -> Vec3:
    x_velocity = math.cos(math.radians(theta)) * PROJECTILE_POWER
    y_velocity = math.sin(math.radians(theta)) * PROJECTILE_POWER
    
    x_distance = x_velocity * time
    y_distance = (y_velocity * time) + ((GRAVITY * (time ** 2)) / 2)
    
    new_pos = Vec3(x_distance + curr.x,
                   y_distance + curr.y)

    return new_pos

