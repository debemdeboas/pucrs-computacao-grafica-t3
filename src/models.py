from src.polygon import Polygon, Triangle
from src.vec3 import Vec3
from dataclasses import dataclass
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from colorir import HexRGB
from src.scenery import draw_cube



@dataclass
class Face(Triangle):
    normals: list[Vec3]
    color: tuple[int, ...] = ()

    def calc_normals(self):
        self.normals = list()
        for v in [self.p1, self.p2, self.p3]:
            normal_vec = Vec3(v.x, v.y, v.z)
            normal_vec.normalize()
            self.normals.append(normal_vec)


@dataclass
class Model:
    vertices: list[Vec3]
    normals: list[Vec3]
    faces: list[Face]
    min_vertex: Vec3 = Vec3(0, 0, 0)
    max_vertex: Vec3 = Vec3(0, 0, 0)

    def get_min_max_vertices(self):
        for v in self.vertices:
            if v.x < self.min_vertex.x:
                self.min_vertex.x = v.x
            if v.y < self.min_vertex.y:
                self.min_vertex.y = v.y
            if v.z < self.min_vertex.z:
                self.min_vertex.z = v.z
                
            if v.x > self.max_vertex.x:
                self.max_vertex.x = v.x
            if v.y > self.max_vertex.y:
                self.max_vertex.y = v.y
            if v.z > self.max_vertex.z:
                self.max_vertex.z = v.z
                

@dataclass
class GameObject:
    def __init__(self, *args, **kwargs) -> None:
        self.model: Model = kwargs['model']
        self.pos: Vec3 = kwargs['pos']
        self.bounding_box: Polygon = kwargs.get('bounding_box', Polygon())
        self.color: tuple[int, ...] = kwargs.get('color', ())
        self.enemy: bool = kwargs.get('enemy', False)
        self.destroyed: bool = kwargs.get('destroyed', False)
        self.scale: tuple[float, ...] = kwargs.get('scale', (1, 1, 1))
        self.pos = self.pos * Vec3(*self.scale)


    def calc_bounding_box(self):
        self.bounding_box.half_size = (self.model.max_vertex - self.model.min_vertex) / 2
        self.bounding_box.center = self.pos + ((self.model.min_vertex + self.model.max_vertex) / 2)
        print(self.bounding_box)

    def draw(self):
        glDisable(GL_TEXTURE_2D)
        glPushMatrix()
        glTranslatef(self.pos.x, self.pos.y, self.pos.z)

        glScalef(*self.scale)

        for face in self.model.faces:
            glBegin(GL_TRIANGLES)

            if self.color:
                glColor3bv(self.color)
            elif face.color and len(face.color) == 3 and all(face.color):
                glColor3bv(face.color)

            glNormal3f(*face.normals[0].unpack())
            glVertex3f(*face.p1.unpack())

            glNormal3f(*face.normals[1].unpack())
            glVertex3f(*face.p2.unpack())

            glNormal3f(*face.normals[2].unpack())
            glVertex3f(*face.p3.unpack())
            glEnd()
        glPopMatrix()
        glEnable(GL_TEXTURE_2D)

    def draw_bounding_box(self):
        # glPushMatrix()
        # glTranslatef(self.pos.x, self.pos.y, self.pos.z)
        # draw_cube(-1, max(*self.bounding_box.half_size.unpack()))
        # glPopMatrix()
        glDisable(GL_TEXTURE_2D)
        
        glPushMatrix()
        glLoadIdentity()
        glColor3bv((255, 255, 255))
        glLineWidth(5)
        
        v = []
        
        v.append((self.bounding_box.center.x - self.bounding_box.half_size.x,
                 self.bounding_box.center.y + self.bounding_box.half_size.y,
                 self.bounding_box.center.z + self.bounding_box.half_size.z))
                    
        v.append((self.bounding_box.center.x - self.bounding_box.half_size.x,
                 self.bounding_box.center.y - self.bounding_box.half_size.y,
                 self.bounding_box.center.z + self.bounding_box.half_size.z))
                    
        v.append((self.bounding_box.center.x + self.bounding_box.half_size.x,
                 self.bounding_box.center.y - self.bounding_box.half_size.y,
                 self.bounding_box.center.z + self.bounding_box.half_size.z))
                    
        v.append((self.bounding_box.center.x + self.bounding_box.half_size.x,
                 self.bounding_box.center.y + self.bounding_box.half_size.y,
                 self.bounding_box.center.z + self.bounding_box.half_size.z))

        v.append((self.bounding_box.center.x + self.bounding_box.half_size.x,
                 self.bounding_box.center.y + self.bounding_box.half_size.y,
                 self.bounding_box.center.z - self.bounding_box.half_size.z))

        v.append((self.bounding_box.center.x + self.bounding_box.half_size.x,
                 self.bounding_box.center.y - self.bounding_box.half_size.y,
                 self.bounding_box.center.z - self.bounding_box.half_size.z))

        v.append((self.bounding_box.center.x - self.bounding_box.half_size.x,
                 self.bounding_box.center.y - self.bounding_box.half_size.y,
                 self.bounding_box.center.z - self.bounding_box.half_size.z))

        v.append((self.bounding_box.center.x - self.bounding_box.half_size.x,
                 self.bounding_box.center.y + self.bounding_box.half_size.y,
                 self.bounding_box.center.z - self.bounding_box.half_size.z))

        print(v)
        
        glBegin(GL_LINE_LOOP)

        glVertex3fv(v[1-1])
        glVertex3fv(v[2-1])
        glVertex3fv(v[3-1])
        glVertex3fv(v[4-1])

        glVertex3fv(v[5-1])
        glVertex3fv(v[6-1])
        glVertex3fv(v[7-1])
        glVertex3fv(v[8-1])

        glVertex3fv(v[8-1])
        glVertex3fv(v[1-1])
        glVertex3fv(v[4-1])
        glVertex3fv(v[5-1])
        
        glVertex3fv(v[6-1])
        glVertex3fv(v[3-1])
        glVertex3fv(v[2-1])
        glVertex3fv(v[7-1])
        
        glVertex3fv(v[4-1])
        glVertex3fv(v[3-1])
        glVertex3fv(v[6-1])
        glVertex3fv(v[5-1])

        glVertex3fv(v[8-1])
        glVertex3fv(v[7-1])
        glVertex3fv(v[2-1])
        glVertex3fv(v[1-1])
        
        glEnd()
        glLineWidth(1)
        glEnable(GL_TEXTURE_2D)
        glPopMatrix()
    

def load_model_tri(filename: str) -> Model:
    with open(filename) as f:
        lines = f.readlines()

    if lines[0].startswith('#OBJETO'):
        lines.pop(0)
        lines.pop(0)
        lines.pop(0)

    m = Model([], [], [])
    for line in lines:
        vecs_line = line.split('  ')
        # color = iter(vecs_line[-1][2:])
        # color = tuple(map((lambda st: int(st[0] + st[1], 16)), (zip(color, color))))
        # color = tuple(map((lambda st: int(st[0] + st[1], 16)), (zip(color, color))))
        # color = vecs_line[-1][2:].strip()
        # color = tuple(int(color[i: i + 2], 16) for i in (0, 2, 4))
        try:
            color = HexRGB(vecs_line[-1][2:].strip()).rgb()
        except:
            color = (0, 0, 0)
        vecs_line.pop()       
        vecs: list[Vec3] = []
        for vec_line in vecs_line:
            curr_vec = vec_line.split()
            vec = Vec3(float(curr_vec[0]),
                       float(curr_vec[1]),
                       float(curr_vec[2]))
            vecs.append(vec)
        face = Face(vecs[0], vecs[1], vecs[2], color=color, normals=[])
        face.calc_normals()
        m.faces.append(face)
    
    m.get_min_max_vertices()
    return m


