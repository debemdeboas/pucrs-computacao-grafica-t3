from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

from src.globals import *
from src.constants import MinMax
from src.textures import *


def draw_axis():
    glPushMatrix()
    glLineWidth(3)
    glBegin(GL_LINES)
    glColor3f (1.0, 1.0, 0.0)
    glVertex3f(0.0, 0.0, 0.0)
    glVertex3f(100, 0.0, 0.0)

    glColor3f (1.0, 1.0, 0.0)
    glVertex3f(0.0, 0.0, 0.0)
    glVertex3f(0.0, 100, 0.0)

    glColor3f (1.0, 1.0, 0.0)
    glVertex3f(0.0, 0.0, 0.0)
    glVertex3f(0.0, 0.0, 100)
    glEnd()
    glPopMatrix()
    glLineWidth(1)


def draw_cube(texture_id: int, size: float):
    half_size = size / 2
    
    cube_faces = [
        (-half_size,  half_size,  half_size),
        (-half_size, -half_size,  half_size),
        ( half_size, -half_size,  half_size),
        ( half_size,  half_size,  half_size),
        ( half_size,  half_size, -half_size),
        ( half_size, -half_size, -half_size),
        (-half_size, -half_size, -half_size),
        (-half_size,  half_size, -half_size),
    ]

    use_texture(texture_id)
    glBegin(GL_QUADS)

    # Front
    glNormal3f(0, 0, 1)
    glTexCoord2f(0, 1)
    glVertex3fv(cube_faces[0])
    glTexCoord2f(0, 0)
    glVertex3fv(cube_faces[1])
    glTexCoord2f(1, 0)
    glVertex3fv(cube_faces[2])
    glTexCoord2f(1, 1)
    glVertex3fv(cube_faces[3])
    
    # Back
    glNormal3f(0, 0, -1)
    glTexCoord2f(0, 1)
    glVertex3fv(cube_faces[4])
    glTexCoord2f(0, 0)
    glVertex3fv(cube_faces[7])
    glTexCoord2f(1, 0)
    glVertex3fv(cube_faces[6])
    glTexCoord2f(1, 1)
    glVertex3fv(cube_faces[5])
    
    # Top
    glNormal3f(0, 1, 0)
    glTexCoord2f(0, 1)
    glVertex3fv(cube_faces[7])
    glTexCoord2f(0, 0)
    glVertex3fv(cube_faces[0])
    glTexCoord2f(1, 0)
    glVertex3fv(cube_faces[3])
    glTexCoord2f(1, 1)
    glVertex3fv(cube_faces[4])
    
    # Bottom
    glNormal3f(0, -1, 0)
    glTexCoord2f(0, 1)
    glVertex3fv(cube_faces[5])
    glTexCoord2f(0, 0)
    glVertex3fv(cube_faces[2])
    glTexCoord2f(1, 0)
    glVertex3fv(cube_faces[1])
    glTexCoord2f(1, 1)
    glVertex3fv(cube_faces[6])
    
    # Right
    glNormal3f(1, 0, 0)
    glTexCoord2f(0, 1)
    glVertex3fv(cube_faces[3])
    glTexCoord2f(0, 0)
    glVertex3fv(cube_faces[2])
    glTexCoord2f(1, 0)
    glVertex3fv(cube_faces[5])
    glTexCoord2f(1, 1)
    glVertex3fv(cube_faces[4])
    
    # Left
    glNormal3f(-1, 0, 0)
    glTexCoord2f(0, 1)
    glVertex3fv(cube_faces[7])
    glTexCoord2f(0, 0)
    glVertex3fv(cube_faces[6])
    glTexCoord2f(1, 0)
    glVertex3fv(cube_faces[1])
    glTexCoord2f(1, 1)
    glVertex3fv(cube_faces[0])
    
    glEnd()
    

def draw_square(texture_id: int):
    use_texture(texture_id)
    glBegin(GL_QUADS)

    glNormal3f(0, 1, 0)
    
    glTexCoord2f(0, 1)
    glVertex3f(0, 1, 0)
    
    glTexCoord2f(0, 0)
    glVertex3f(0, 0, 0)
    
    glTexCoord2f(1, 0)
    glVertex3f(1, 0, 0)

    glTexCoord2f(1, 1)
    glVertex3f(1, 1, 0)
    glEnd()
    

def draw_walls(texture_id: int, width: MinMax, height: MinMax, depth: MinMax):
    # Front wall
    glPushMatrix()
    glTranslatef(-0.5, 0, -0.5)
    glScalef(width.max + 1, height.max, depth.min)
    # glColor3ub(180, 20, 25)
    draw_square(texture_id)
    glPopMatrix()

    # Back wall
    glPushMatrix()
    glTranslatef(-0.5, 0, 25.5)
    glScalef(width.max + 1, height.max, depth.min)
    # glColor3ub(180, 20, 25)
    draw_square(texture_id)
    glPopMatrix()

    # Left wall
    glPushMatrix()
    glTranslatef(-0.5, 0, -0.5)
    glRotatef(90, 0, 1, 0)
    glScalef(-depth.max - 1, height.max, depth.min)
    # glColor3ub(180, 20, 25)
    draw_square(texture_id)
    glPopMatrix()

    # Right wall
    glPushMatrix()
    glTranslatef(50.5, 0, -0.5)
    glRotatef(90, 0, 1, 0)
    glScalef(-depth.max - 1, height.max, depth.min)
    # glColor3ub(180, 20, 25)
    draw_square(texture_id)
    glPopMatrix()


# **********************************************************************
# void DesenhaLadrilho(int corBorda, int corDentro)
# **********************************************************************
def draw_tile():
    """
    Desenha uma célula do piso.

    O ladrilho tem largura 1, centro no (0, 0, 0) e está sobre o plano XZ.
    """

    glColor3f(1, 1, 1) # desenha QUAD em branco, pois vai usa textura
    glBegin(GL_QUADS)
    glNormal3f(0, 1, 0)
    glTexCoord(0, 0)
    glVertex3f(-0.5, 0.0, -0.5)
    glTexCoord(0, 1)
    glVertex3f(-0.5, 0.0, 0.5)
    glTexCoord(1, 1)
    glVertex3f(0.5, 0.0, 0.5)
    glTexCoord(1, 0)
    glVertex3f(0.5, 0.0, -0.5)
    glEnd()

    glColor3f(1,1,1) # desenha a borda da QUAD
    glBegin(GL_LINE_STRIP)
    glNormal3f(0, 1, 0)
    glVertex3f(-0.5, 0.0, -0.5)
    glVertex3f(-0.5, 0.0, 0.5)
    glVertex3f(0.5, 0.0, 0.5)
    glVertex3f(0.5, 0.0, -0.5)
    glEnd()


def draw_floor_ex():
    glPushMatrix()
    glTranslated(-20, -1, -10)
    for _ in range(-20, 20):
        glPushMatrix()
        for z in range(-20, 20):
            if z % 2 == 0:
                use_texture(0)
            else:
                use_texture(1)
            draw_tile()
            glTranslated(0, 0, 1)
        glPopMatrix()
        glTranslated(1, 0, 0)
    glPopMatrix()


def draw_floor():
    glPushMatrix()
    for _ in range(50 + 1):
        glPushMatrix()
        for _ in range(25 + 1):
            use_texture(2)
            draw_tile()
            glTranslated(0, 0, 1)
        glPopMatrix()
        glTranslated(1, 0, 0)
    glPopMatrix()


def draw_division_wall(texture_id):
    for block in g_division_wall:
        if block.destroyed:
            continue
        
        glPushMatrix()
        glTranslatef(block.pos.x, block.pos.y, block.pos.z)
        draw_cube(texture_id, 0.99)
        glPopMatrix()


def draw_scenario():
    draw_floor()
    use_texture(-1)
    draw_walls(3,
               MinMax(MIN_WIDTH, MAX_WIDTH),
               MinMax(MIN_HEIGHT, MAX_HEIGHT),
               MinMax(MIN_DEPTH, MAX_DEPTH))
    use_texture(-1)
    draw_division_wall(0)
