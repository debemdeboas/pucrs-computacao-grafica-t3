# ***********************************************************************************
#   OpenGLBasico3D-V5.py
#       Autor: Márcio Sarroglia Pinho
#       pinho@pucrs.br
#   Este programa exibe dois Cubos em OpenGL
#   Para maiores informações, consulte
#
#   Para construir este programa, foi utilizada a biblioteca PyOpenGL, disponível em
#   http://pyopengl.sourceforge.net/documentation/index.html
#
#   Outro exemplo de código em Python, usando OpenGL3D pode ser obtido em
#   http://openglsamples.sourceforge.net/cube_py.html
#
#   Sugere-se consultar também as páginas listadas
#   a seguir:
#   http://bazaar.launchpad.net/~mcfletch/pyopengl-demo/trunk/view/head:/PyOpenGL-Demo/NeHe/lesson1.py
#   http://pyopengl.sourceforge.net/documentation/manual-3.0/index.html#GLUT
#
#   No caso de usar no MacOS, pode ser necessário alterar o arquivo ctypesloader.py,
#   conforme a descrição que está nestes links:
#   https://stackoverflow.com/questions/63475461/unable-to-import-opengl-gl-in-python-on-macos
#   https://stackoverflow.com/questions/6819661/python-location-on-mac-osx
#   Veja o arquivo Patch.rtf, armazenado na mesma pasta deste fonte.
#
# ***********************************************************************************

import OpenGL

OpenGL.ERROR_CHECKING = False

import random
import time

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

from src.globals import *
from src.models import *
from src.point import Point
from src.polygon import *
from src.scenery import draw_axis, draw_scenario
from src.textures import load_texture
from src.vec3 import Vec3


def calc_point(p: Point) -> Point:
    """
    Esta função calcula as coordenadas
    de um ponto no sistema de referência da
    camera (SRC), ou seja, aplica as rotações,
    escalas e translações a um ponto no sistema
    de referência do objeto SRO.
    Para maiores detalhes, veja a página
    https://www.inf.pucrs.br/pinho/CG/Aulas/OpenGL/intersec/ExerciciosDeintersec.html
    """

    new_point = [0, 0, 0, 0]

    mvmatrix = glGetDoublev(GL_MODELVIEW_MATRIX)
    for i in range(0, 4):
        new_point[i] = mvmatrix[0][i] * p.x + \
                       mvmatrix[1][i] * p.y + \
                       mvmatrix[2][i] * p.z + \
                       mvmatrix[3][i]

    x = new_point[0]
    y = new_point[1]
    z = -new_point[2]
    #print ("Ponto na saída:", ponto_novo)
    return Point(x, y, z)


def init():
    global g_textures, g_camera, g_models, g_objects

    # Define a cor do fundo da tela
    glClearColor(0, 0, 0.54, 1.0)

    glClearDepth(1)
    glDepthFunc(GL_LESS)
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE)
    glShadeModel(GL_SMOOTH)
    glEnable(GL_DEPTH_TEST)
    glEnable(GL_LINE_SMOOTH)
    glEnable(GL_NORMALIZE)
    # glEnable(GL_CULL_FACE)
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)

    # Carrega texturas
    g_textures.append(load_texture('textures/bricks.jpg'))
    g_textures.append(load_texture('textures/Piso.jpg'))
    g_textures.append(load_texture('textures/grass.jpg'))
    g_textures.append(load_texture('textures/walnut.jpg'))

    # Load objects
    g_models.append(load_model_tri('3dobjects/tri/casa.tri'))
    g_models.append(load_model_tri('3dobjects/tri/dog.tri'))
    g_models.append(load_model_tri('3dobjects/tri/cactus.tri'))

    # Define posição da câmera
    g_camera.set_pos(Vec3(MAX_WIDTH / 2, MAX_HEIGHT * 4, MAX_DEPTH * 1.5),
                     Vec3(MAX_WIDTH / 2, -0.6, (MAX_DEPTH / 3) - 2))

    for y in range(DIV_WALL_HEIGHT):
        z = 0
        for _ in range(MAX_DEPTH + 1):
            pos = Vec3(MAX_WIDTH / 2, y + 0.5, z)
            wall_block = WallBlock(pos)
            wall_block.destroyed = random.choice([True, False])
            g_division_wall.append(wall_block)        
            z += 1
         
    x = 2
    y = 0
    z = 1
    for _ in range(5):
        obj = GameObject(model=g_models[1],
                         pos=Vec3(x, y, z))
        obj.scale = (0.5, 0.5, 0.5)
        obj.calc_bounding_box()
        g_objects.append(obj)
        z += 4
        
    x = 6
    y = 0
    z = 1
    for _ in range(5):
        obj = GameObject(model=g_models[1],
                         pos=Vec3(x, y, z))
        obj.scale = (0.2, 0.2, 0.2)
        obj.calc_bounding_box()
        g_objects.append(obj)
        z += 4

    x = 28
    y = 0
    z = 1
    for _ in range(5):
        obj = GameObject(model=g_models[1],
                         pos=Vec3(x, y, z))
        obj.scale = (0.2, 0.2, 0.2)
        obj.calc_bounding_box()
        g_objects.append(obj)
        z += 4
        
    x = 32
    y = 0
    z = 1
    for _ in range(5):
        obj = GameObject(model=g_models[1],
                         pos=Vec3(x, y, z))
        obj.scale = (0.2, 0.2, 0.2)
        obj.calc_bounding_box()
        g_objects.append(obj)
        z += 4


# **********************************************************************
# PosicUser()
# **********************************************************************
def position_user():
    global g_aspect_ratio

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(60, g_aspect_ratio, 0.01, 100) # Projeção perspectiva

    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    gluLookAt(0, 4, 10, 0, 0, 0, 0, 1, 0)


# **********************************************************************
#  reshape( w: int, h: int )
#  trata o redimensionamento da janela OpenGL
# **********************************************************************
def reshape(w: int, h: int):
    global g_aspect_ratio

	# Evita divisão por zero, no caso de uam janela com largura 0.
    if h == 0:
        h = 1

    # Ajusta a relação entre largura e altura para evitar distorção na imagem.
    # Veja função "position_user".
    g_aspect_ratio = w / h
	# Reset the coordinate system before modifying
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    # Seta a viewport para ocupar toda a janela
    glViewport(0, 0, w, h)

    position_user()


# **********************************************************************
# DefineLuz()
# **********************************************************************
def define_lighting():
    glPushMatrix()
    glLoadIdentity()

    # Define cores para um objeto dourado
    ambient_light = [0.5, 0.5, 0.5]
    diffuse_light = [0.7, 0.7, 0.7]
    specular_light = [0.6, 0.6, 0.6]
    specularity = [0.5, 0.5, 0.8]
    light_pos_0 = [MAX_WIDTH * 1.2, MAX_HEIGHT * 4, MAX_DEPTH / 2]  # Posição da Luz

    # Fonte de Luz 0
    glEnable(GL_COLOR_MATERIAL)

    # Habilita o uso de iluminação
    glEnable(GL_LIGHTING)

    # Ativa o uso da luz ambiente
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambient_light)
    # Define os parâmetros da luz numero Zero
    glLightfv(GL_LIGHT0, GL_AMBIENT, ambient_light)
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse_light)
    glLightfv(GL_LIGHT0, GL_SPECULAR, specular_light)
    glLightfv(GL_LIGHT0, GL_POSITION, light_pos_0)
    glEnable(GL_LIGHT0)

    # Ativa o "Color Tracking"
    glEnable(GL_COLOR_MATERIAL)

    # Define a reflexão do material
    glMaterialfv(GL_FRONT, GL_SPECULAR, specularity)

    # Define a concentração do brilho.
    # Quanto maior o valor do Segundo parâmetro, mais
    # concentrado será o brilho. (Valores validos: de 0 a 128)
    glMateriali(GL_FRONT, GL_SHININESS, 80)
    glPopMatrix()


def display():
    global g_camera
    global g_angle

    # Limpa a tela com  a cor de fundo
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)  # type: ignore
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    g_camera.activate()
    define_lighting()

    draw_axis()
    draw_scenario()
    draw_game_objects()

    g_angle += 1
    glutSwapBuffers()


def draw_game_objects():
    global g_objects
    for object in g_objects:
        object.draw()
        object.draw_bounding_box()
    

def animate():
    """
    Função chama enquanto o programa está ocioso.

    Calcula o FPS e numero de intersecção detectadas, junto com outras informações.
    """

    global g_n_frames, g_total_time, g_delta_t_accum, g_old_time, g_fps_cap

    now_time = time.time()
    dt = now_time - g_old_time
    g_old_time = now_time

    g_delta_t_accum += dt
    g_total_time += dt
    g_n_frames += 1

    if g_delta_t_accum > 1.0 / g_fps_cap:  # fixa a atualização da tela em g_fps_cap FPS
        g_delta_t_accum = 0
        glutPostRedisplay()


def keyboard(*args):  # (key: int, x: int, y: int)
    global g_camera
    #print(args)

    # If escape is pressed, kill everything.
    match args[0]:
        case b'\x1b':
            os._exit(0)
        case b'w' | b'W':
            g_camera.move_forward()
        case b's' | b'S':
            g_camera.move_backward()
        case b'a' | b'A':
            g_camera.move_left()
        case b'd' | b'D':
            g_camera.move_right()
        case b'q'| b'Q':
            g_camera.move_up()
        case b'e' | b'E':
            g_camera.move_down()
        case b' ':
            init()
        case b'p':
            print(g_camera.pitch, g_camera.yaw, g_camera.pos, g_camera.direction)

    # Força o redesenho da tela
    glutPostRedisplay()


def arrow_keys(a_keys: int, x: int, y: int):
    global g_camera

    if a_keys == GLUT_KEY_UP:    # GO TO TOP VIEW
        g_camera.set_pos(
            Vec3(MAX_WIDTH / 2, MAX_HEIGHT * 12, MAX_DEPTH / 2),
            Vec3(0, -0xffff, 0)
        )
        g_camera.pitch = 449
        g_camera.yaw = 0
        g_camera.look_at(Vec3(0, 0, 0))
        g_camera.update()
    if a_keys == GLUT_KEY_DOWN:  # GO TO BOTTOM VIEW
        g_camera.pitch = 0
        g_camera.yaw = 0
    if a_keys == GLUT_KEY_LEFT:  # GO TO LEFT VIEW
        g_camera.set_pos(
            Vec3(MAX_WIDTH / 2, (MAX_HEIGHT + WALL_HEIGHT)/ 2, MAX_DEPTH * 2),
            Vec3((MAX_WIDTH / 2) * 0xff, 0, 0)
        )
        g_camera.pitch = 0
        g_camera.yaw = 0
        g_camera.look_at(Vec3(0, 0, 0))
        g_camera.update()
    if a_keys == GLUT_KEY_RIGHT: # GO TO RIGHT VIEW
        g_camera.pitch = 0
        g_camera.yaw = 0
    glutPostRedisplay()


def mouse(button: int, state: int, x: int, y: int):
    global g_last_mouse_pos_x, g_last_mouse_pos_y

    if state == 0:
        g_last_mouse_pos_x = x
        g_last_mouse_pos_y = y
    glutPostRedisplay()


def mouse_move(x: int, y: int):
    global g_last_mouse_pos_x, g_last_mouse_pos_y
    global g_camera, g_mouse_move_threshold

    delta_x = x - g_last_mouse_pos_x
    if delta_x > 40:
        delta_x = 40
    g_camera.update_yaw(delta_x)
    g_last_mouse_pos_x = float(x)

    delta_y = y - g_last_mouse_pos_y
    if delta_y > 40:
        delta_y = 40
    g_camera.update_pitch(delta_y)
    g_last_mouse_pos_y = float(y)

    g_camera.update()
    glutPostRedisplay()


if __name__ == '__main__':
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_RGB)  # type: ignore
    glutInitWindowPosition(0, 0)

    # Define o tamanho inicial da janela gráfica do programa
    glutInitWindowSize(1080, 700)
    # Cria a janela na tela, definindo o nome da
    # que aparecera na barra de tÃ­tulo da janela.
    glutInitWindowPosition(100, 100)
    wind = glutCreateWindow('Robô Articulado Atirador - Trabalho III')

    # executa algumas inicializações
    init()

    # Define que o tratador de evento para
    # o redesenho da tela. A função "display"
    # será chamada automaticamente quando
    # for necessário redesenhar a janela
    glutDisplayFunc(display)
    glutIdleFunc(animate)

    # o redimensionamento da janela. A função "reshape"
    # Define que o tratador de evento para
    # será chamada automaticamente quando
    # o usuário alterar o tamanho da janela
    glutReshapeFunc(reshape)

    # Define que o tratador de evento para
    # as teclas. A função "keyboard"
    # será chamada automaticamente sempre
    # o usuário pressionar uma tecla comum
    glutKeyboardFunc(keyboard)

    # Define que o tratador de evento para
    # as teclas especiais(F1, F2,... ALT-A,
    # ALT-B, Teclas de Seta, ...).
    # A função "arrow_keys" será chamada
    # automaticamente sempre o usuário
    # pressionar uma tecla especial
    glutSpecialFunc(arrow_keys)

    glutMouseFunc(mouse)
    glutMotionFunc(mouse_move)

    try:
        # inicia o tratamento dos eventos
        glutMainLoop()
    except SystemExit:
        print('Ending')
        raise
